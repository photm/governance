# Changelog

All notable changes to Website Circle Policies are documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and versioning is based solely on date.

## 2018-08-01
### Changed
- Introduced this Changelog.
