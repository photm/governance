# Proposals and consent polls on the forum

The following initially came itself from a proposal on the forum:
<https://community.snowdrift.coop/t/process-for-consent-decisions-on-the-forum/1443>

## How to create a proposal

* If possible: Before coming up with solutions for a problem, write about the problem on the forum and get feedback. Make sure everyone agrees it's a problem that should be solved.
* Write a proposal for something simple to try, even if it doesn't solve the problem completely. Don't waste time on this step.  Timebox for a few hours, write the best proposal you can write in this very short time, and get it out there soon.
* Post your proposal on the forum and ask for feedback; give it some time and integrate the feedback. (If you're unsure, take one week for this phase and then simply try a consent decision.)

## How to start a consent decision

Any forum user with sufficient rights to build a poll can start a consent decision. To start one, write a forum post with these contents:

* Your proposal, or a reference to the proposal. Make sure to specify the last-edited timestamp (or number of times they have been edited) of posts you refer to.
* A proposed review date
* A poll, created using the :gear: gear tool in the editor. Use these settings for the poll:

> * Type: Single Choice
> * Results: Always
> * Chart type: Bar
> * Poll options:
>   * *Objection (explain it in a reply)*
>   * *Consent with Concern (explain it in a reply)*
>   * *Consent*
> * Show who voted: Yes
> * Automatically close poll: Yes, after one week

If there's no objections once the poll closes, the decision becomes effective immediately.

At the review date of a decision, the secretary will post an invitation for reflection and discussion. One week later, the secretary will start a consent decision on whether to keep the decision until the next review (they will propose the next review date as part of that consent decision).

## How to integrate objections

Once there's an objection:

* Discuss possible ways to integrate the objection with the objector and the entire group
* Give the others enough time to propose amendments, instead of rushing to amend it yourself
* Once there's a good amendment:  Close the poll and edit the proposal. Then start a consent decision on the new proposal as described above.

## How to participate in a consent decision

All forum users may participate in consent decisions. To participate, do the following:

* Read the proposal thoroughly, ask questions if you don't understand everything
* Try to find an [objection (S3 pattern)](https://patterns.sociocracy30.org/objection.html) to the proposal
* Vote in the poll
* If you have an objection or a concern, explain it in a reply to the post that contains the poll.
* Participate in any follow-up discussion. Contribute ideas for improving the proposal in a way that fixes your objection.

Objections can be on any of these grounds:
* The proposal is not clear enough
* The proposal doesn't contain a review date
* The proposal would lead to unintended consequences
* An important improvement to the proposal can be made
* The consent decision process (as described in this post) was not followed correctly
* The proposal is not inside the domain of what can be decided on the forum

## How to deal with invalid objections

Only people in [the team (forum group)](https://community.snowdrift.coop/g/team) can formally disqualify objections. Here's how to proceed:

* Try to integrate the objection anyways. If there's a good way to integrate it, there's no need to disqualify the objection.
* Discuss the matter with the objector. If you can convince them while the poll is still open, they can still change their vote.  If they're convinced only after the poll is closed, open a new poll. Be open to the possibility that the objection is valid after all.
* If the matter really cannot be resolved differently, here's how an objection can be disqualified formally:

> * A team member starts a consent decision, proposing to disqualify the objection. Give a good reason, i.e. an objection to the objection.  Make it clear that only team members can participate in this consent decision.
> * Open a new poll for the original proposal that was objected to.  Make it clear that the original objection cannot be raised again.  Be friendly and open to other objections from the original objector.

## The power remains with the team

The team invites all forum users to participate in consent decisions. However, it reserves the right to withdraw this invitation at any point.

When team members start consent decisions, they have the option to specify that only team members can raise objections. If they want to permanently withdraw the invitation, a consent decision can be made to remove the invitation from this very proposal.
